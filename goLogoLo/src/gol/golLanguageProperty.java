package gol;

/**
 * This class provides the properties that are needed to be loaded from
 * language-dependent XML files.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public enum golLanguageProperty {
    SELECTION_TOOL_ICON,
    SELECTION_TOOL_TOOLTIP,
    
    REMOVE_ICON,
    REMOVE_ELEMENT_TOOLTIP,
    
    RECTANGLE_ICON,
    RECTANGLE_TOOLTIP,
    
    ELLIPSE_ICON,
    ELLIPSE_TOOLTIP,
    
    MOVE_TO_BACK_ICON,
    MOVE_TO_BACK_TOOLTIP,
    MOVE_TO_FRONT_ICON,
    MOVE_TO_FRONT_TOOLTIP,
    
    BACKGROUND_TOOLTIP,
    FILL_TOOLTIP,
    OUTLINE_TOOLTIP,
    
    SNAPSHOT_ICON,
    SNAPSHOT_TOOLTIP,
    
    FONT_FAMILY_TOOLTIP,
    FONT_SIZE_TOOLTIP,
    BOLDEN_TOOLTIP,
    ITALICIZE_TOOLTIP
}