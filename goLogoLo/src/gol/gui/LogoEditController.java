package gol.gui;

import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;
import gol.data.golData;
import gol.data.golState;
import gol.gui.golWorkspace;
import djf.AppTemplate;
import gol.data.DraggableEllipse;
import gol.data.DraggableImageView;
import gol.data.DraggableRectangle;
import gol.data.DraggableText;
import java.net.MalformedURLException;
import java.util.Optional;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

/**
 * This class responds to interactions with other UI logo editing controls.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class LogoEditController {
    AppTemplate app;
    golData dataManager;
    Node clipboardShape;
    
    
    public LogoEditController(AppTemplate initApp) {
	app = initApp;
	dataManager = (golData)app.getDataComponent();
    }
    
    /**
     * This method handles the response for selecting either the
     * selection or removal tool.
     */
    public void processSelectSelectionTool() {
	// CHANGE THE CURSOR
	Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.DEFAULT);
	
	// CHANGE THE STATE
	dataManager.setState(golState.SELECTING_SHAPE);	
	
	// ENABLE/DISABLE THE PROPER BUTTONS
	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * This method handles a user request to remove the selected shape.
     */
    public void processRemoveSelectedShape() {
	// REMOVE THE SELECTED SHAPE IF THERE IS ONE
        RemoveNode_Transaction remove = new RemoveNode_Transaction(dataManager.getSelectedShape(),app);
	dataManager.removeSelectedShape();
	
	// ENABLE/DISABLE THE PROPER BUTTONS
	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
        workspace.getJTPS().addTransaction(remove);
	app.getGUI().updateToolbarControls(false);
    }
    
    /**
     * This method processes a user request to start drawing a rectangle.
     */
    public void processSelectRectangleToDraw() {
	// CHANGE THE CURSOR
	Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.CROSSHAIR);
	
	// CHANGE THE STATE
	dataManager.setState(golState.STARTING_RECTANGLE);

	// ENABLE/DISABLE THE PROPER BUTTONS
	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * This method provides a response to the user requesting to start
     * drawing an ellipse.
     */
    public void processSelectEllipseToDraw() {
	// CHANGE THE CURSOR
	Scene scene = app.getGUI().getPrimaryScene();
	scene.setCursor(Cursor.CROSSHAIR);
	
	// CHANGE THE STATE
	dataManager.setState(golState.STARTING_ELLIPSE);

	// ENABLE/DISABLE THE PROPER BUTTONS
	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace(dataManager);
    }
    
    /**
     * This method processes a user request to move the selected shape
     * down to the back layer.
     */
    public void processMoveSelectedShapeToBack() {
        MoveBack_Transaction back = new MoveBack_Transaction(dataManager.getSelectedShape(),app);
        golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
	workspace.getJTPS().addTransaction(back);
	dataManager.moveSelectedShapeToBack();
        app.getGUI().updateDoToolBarControls(3);
	app.getGUI().updateToolbarControls(false);
    }
    
    /**
     * This method processes a user request to move the selected shape
     * up to the front layer.
     */
    public void processMoveSelectedShapeToFront() {
        MoveFront_Transaction front = new MoveFront_Transaction(dataManager.getSelectedShape(),app);
        golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
	workspace.getJTPS().addTransaction(front);
	dataManager.moveSelectedShapeToFront();
        app.getGUI().updateDoToolBarControls(3);
	app.getGUI().updateToolbarControls(false);
    }
        
    /**
     * This method processes a user request to select a fill color for
     * a shape.
     */
    public void processSelectFillColor() {
	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        Color oldColor = dataManager.getCurrentFillColor();
	Color selectedColor = workspace.getFillColorPicker().getValue();
	if (selectedColor != null) {
	    dataManager.setCurrentFillColor(selectedColor);
            ChangeFillColor_Transaction bgColor = new ChangeFillColor_Transaction(oldColor,selectedColor,app); 
            workspace.getJTPS().addTransaction(bgColor);
	    app.getGUI().updateToolbarControls(false);
            app.getGUI().updateDoToolBarControls(3);
	}
    }
    
    /**
     * This method processes a user request to select the outline
     * color for a shape.
     */
    public void processSelectOutlineColor() {
	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        Color oldColor = dataManager.getCurrentOutlineColor();
	Color selectedColor = workspace.getOutlineColorPicker().getValue();
	if (selectedColor != null) {
	    dataManager.setCurrentOutlineColor(selectedColor);
	    ChangeOLColor_Transaction bgColor = new ChangeOLColor_Transaction(oldColor,selectedColor,app); 
            workspace.getJTPS().addTransaction(bgColor);
	    app.getGUI().updateToolbarControls(false);
            app.getGUI().updateDoToolBarControls(3);
	}    
    }
    
    /**
     * This method processes a user request to select the 
     * background color.
     */
    public void processSelectBackgroundColor() {
	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        Color oldColor = dataManager.getBackgroundColor();
	Color selectedColor = workspace.getBackgroundColorPicker().getValue();
	if (selectedColor != null) {
	    dataManager.setBackgroundColor(selectedColor);
            ChangeBGColor_Transaction bgColor = new ChangeBGColor_Transaction(oldColor,selectedColor,app); 
            workspace.getJTPS().addTransaction(bgColor);
	    app.getGUI().updateToolbarControls(false);
            app.getGUI().updateDoToolBarControls(3);
	}
    }
    
    /**
     * This method processes a user request to select the outline
     * thickness for shape drawing.
     */
    public void processSelectOutlineThickness() {
	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
	int outlineThickness = (int)workspace.getOutlineThicknessSlider().getValue();
	dataManager.setCurrentOutlineThickness(outlineThickness);
	app.getGUI().updateToolbarControls(false);
    }
    
    /**
     * This method processes a user request to take a snapshot of the
     * current scene.
     */
    public void processSnapshot() {
	golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
	Pane canvas = workspace.getCanvas();
	WritableImage image = canvas.snapshot(new SnapshotParameters(), null);
	File file = new File("Logo.png");
	try {
	    ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
	}
	catch(IOException ioe) {
	    ioe.printStackTrace();
	}
    }
    public void processEmbedImage() {
        File imgFile = openImage();
        try {
            if (imgFile != null) {
                displayImage(imgFile);
            } 
        } catch (MalformedURLException e) {
            showError(e);
        }
    }
    
    File openImage() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select an Image File");
        fileChooser.getExtensionFilters().add(new ExtensionFilter(
            "Image Files", "*.png", "*.jpg", "*.gif"));
        return fileChooser.showOpenDialog(null);
    }
    
    void displayImage(File imgFile) throws MalformedURLException{
        Image node = new Image(imgFile.toURI().toURL().toExternalForm());
        double width = node.getWidth();
        double height = node.getHeight();
        golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        DraggableImageView nodeView = new DraggableImageView(node);
        nodeView.setPath(imgFile.toURI().toURL().toExternalForm());
        nodeView.setX(canvas.getWidth()/2 - width/2);
        nodeView.setY(canvas.getHeight()/2 - height/2);
        dataManager.addShape(nodeView);
        app.getGUI().updateToolbarControls(false);
        //canvas.getChildren().add(nodeView);
        app.getGUI().updateDoToolBarControls(3);
    }
    
    void showError(Exception e) {
        Alert info = new Alert(AlertType.ERROR);
        info.setTitle("Image not found");
        info.setHeaderText("Image not found");
        info.setContentText(e.getMessage());
        info.showAndWait();
    }
    
    public void processEmbedText() {
        TextInputDialog dialog = new TextInputDialog("goLogoGo");
        dialog.setTitle("Text Input");
        dialog.setContentText("Enter Text:");

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            DraggableText node = new DraggableText(result.toString().substring(9, result.toString().length()-1));
            golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
            Pane canvas = workspace.getCanvas();
            node.setX(canvas.getWidth()/2);
            node.setY(canvas.getHeight()/2);
            dataManager.addShape(node);
            app.getGUI().updateDoToolBarControls(3);
        }
    }

    private DraggableImageView Draggable(ImageView nodeView) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public void processCopy(){
        Copy_Transaction copy = new Copy_Transaction(clipboardShape,dataManager.getSelectedShape(),app);
        clipboardShape = dataManager.getSelectedShape();
        golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        workspace.getJTPS().addTransaction(copy);
        Pane canvas = workspace.getCanvas();
        if (clipboardShape instanceof DraggableRectangle){
            clipboardShape = ((DraggableRectangle)clipboardShape).deepCopy(canvas.getWidth()/2, canvas.getHeight()/2);
        } if (clipboardShape instanceof DraggableEllipse){
            clipboardShape = ((DraggableEllipse)clipboardShape).deepCopy(canvas.getWidth()/2, canvas.getHeight()/2);
        } if (clipboardShape instanceof DraggableImageView){
            clipboardShape = ((DraggableImageView)clipboardShape).deepCopy(canvas.getWidth()/2, canvas.getHeight()/2);
        } if (clipboardShape instanceof DraggableText){
            clipboardShape = ((DraggableText)clipboardShape).deepCopy(canvas.getWidth()/2, canvas.getHeight()/2);
        }
        app.getGUI().updateDoToolBarControls(3);
        app.getGUI().updateDuplicationToolbarControls(1);
    }
    public void processCut(){
        Cut_Transaction copy = new Cut_Transaction(clipboardShape,null,app);
        clipboardShape = dataManager.getSelectedShape();
        golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        if (clipboardShape instanceof DraggableRectangle){
            clipboardShape = ((DraggableRectangle)clipboardShape).deepCopy(canvas.getWidth()/2, canvas.getHeight()/2);
        } if (clipboardShape instanceof DraggableEllipse){
            clipboardShape = ((DraggableEllipse)clipboardShape).deepCopy(canvas.getWidth()/2, canvas.getHeight()/2);
        } if (clipboardShape instanceof DraggableImageView){
            clipboardShape = ((DraggableImageView)clipboardShape).deepCopy(canvas.getWidth()/2, canvas.getHeight()/2);
        } if (clipboardShape instanceof DraggableText){
            clipboardShape = ((DraggableText)clipboardShape).deepCopy(canvas.getWidth()/2, canvas.getHeight()/2);
        }
        copy.setNewCopy(clipboardShape);
        dataManager.removeSelectedShape();
        workspace.getJTPS().addTransaction(copy);
	// ENABLE/DISABLE THE PROPER BUTTONS
	workspace.reloadWorkspace(dataManager);
        app.getGUI().updateDoToolBarControls(3);
        app.getGUI().updateDuplicationToolbarControls(1);
	app.getGUI().updateToolbarControls(false);
    }
    public void processPaste(){
        golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
        Pane canvas = workspace.getCanvas();
        Paste_Transaction paste;
        if (clipboardShape instanceof DraggableEllipse){
            //dataManager.addShape(clipboardShape);
            canvas.getChildren().add(clipboardShape);
            paste = new Paste_Transaction(clipboardShape,app);
            
            clipboardShape = ((DraggableEllipse)clipboardShape).deepCopy(canvas.getWidth()/2, canvas.getHeight()/2);
        } else if (clipboardShape instanceof DraggableRectangle){
            //dataManager.addShape(clipboardShape);
            canvas.getChildren().add(clipboardShape);
            paste = new Paste_Transaction(clipboardShape,app);
            clipboardShape = ((DraggableRectangle)clipboardShape).deepCopy(canvas.getWidth()/2, canvas.getHeight()/2);
        } else if (clipboardShape instanceof DraggableImageView){
            //dataManager.addShape(clipboardShape);
            canvas.getChildren().add(clipboardShape);
            paste = new Paste_Transaction(clipboardShape,app);
            clipboardShape = ((DraggableImageView)clipboardShape).deepCopy(canvas.getWidth()/2, canvas.getHeight()/2);
        } else {
            //dataManager.addShape(clipboardShape);
            canvas.getChildren().add(clipboardShape);
            paste = new Paste_Transaction(clipboardShape,app);
            clipboardShape = ((DraggableText)clipboardShape).deepCopy(canvas.getWidth()/2, canvas.getHeight()/2);
        }
        workspace.getJTPS().addTransaction(paste);
    }
    public Node getClipboard(){
        return clipboardShape;
    }
    
    public void setClipboard(Node newClip){
        clipboardShape = newClip;
    }
}
