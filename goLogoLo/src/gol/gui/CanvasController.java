package gol.gui;

import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.shape.Shape;
import gol.data.golData;
import gol.data.Draggable;
import gol.data.golState;
import static gol.data.golState.DRAGGING_NOTHING;
import static gol.data.golState.DRAGGING_SHAPE;
import static gol.data.golState.SELECTING_SHAPE;
import static gol.data.golState.SIZING_SHAPE;
import djf.AppTemplate;
import gol.data.DraggableEllipse;
import gol.data.DraggableImageView;
import gol.data.DraggableRectangle;
import gol.data.DraggableText;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.scene.Node;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.TextInputDialog;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;

/**
 * This class responds to interactions with the rendering surface.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class CanvasController {

    AppTemplate app;
    static double initialx;
    static double initialy;
    DragNode_Transaction drag;

    public CanvasController(AppTemplate initApp) {
        app = initApp;
    }

    /**
     * Respond to mouse presses on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMousePress(int x, int y) {
        golData dataManager = (golData) app.getDataComponent();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            // SELECT THE TOP SHAPE
            //System.out.println("xd");
            Node shape = dataManager.selectTopShape(x, y);
            if (shape instanceof DraggableRectangle){
                drag = new DragNode_Transaction(((DraggableRectangle)shape).xProperty().doubleValue(),((DraggableRectangle)shape).yProperty().doubleValue(),0,0,app);
            } else if (shape instanceof DraggableEllipse){
                drag = new DragNode_Transaction(((DraggableEllipse)shape).centerXProperty().doubleValue(),((DraggableEllipse)shape).centerXProperty().doubleValue(),0,0,app);
            } else if (shape instanceof DraggableImageView){
                drag = new DragNode_Transaction(((DraggableImageView)shape).xProperty().doubleValue(),((DraggableImageView)shape).yProperty().doubleValue(),0,0,app);
            } else {
                drag = new DragNode_Transaction(((DraggableText)shape).xProperty().doubleValue(),((DraggableText)shape).xProperty().doubleValue(),0,0,app);
            }
            app.getGUI().updateDoToolBarControls(3);
            golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
            workspace.getJTPS().addTransaction(drag);
            Scene scene = app.getGUI().getPrimaryScene();

            // AND START DRAGGING IT
            if (shape != null) {
                app.getGUI().updateDuplicationToolbarControls(0);
                scene.setCursor(Cursor.MOVE);
                dataManager.setState(golState.DRAGGING_SHAPE);
                Draggable selectedDraggableShape = (Draggable) dataManager.getSelectedShape();
                initialx = x - selectedDraggableShape.getX();
                initialy = y - selectedDraggableShape.getY();
                
                System.out.print("uh");
                app.getGUI().updateToolbarControls(false);
            } else {
                scene.setCursor(Cursor.DEFAULT);
                dataManager.setState(DRAGGING_NOTHING);
                app.getWorkspaceComponent().reloadWorkspace(dataManager);
            }
        } else if (dataManager.isInState(golState.STARTING_RECTANGLE)) {
            dataManager.startNewRectangle(x, y);
        } else if (dataManager.isInState(golState.STARTING_ELLIPSE)) {
            dataManager.startNewEllipse(x, y);
        }
        golWorkspace workspace = (golWorkspace) app.getWorkspaceComponent();
        workspace.reloadWorkspace(dataManager);
    }
    public void processCanvasMouseDoubleClick(int x, int y){
        golData dataManager = (golData) app.getDataComponent();
        if (dataManager.isInState(SELECTING_SHAPE)) {
            Node shape = dataManager.selectTopShape(x, y);
            if (shape instanceof DraggableText){
                String temp = ((DraggableText) shape).getText();
                TextInputDialog dialog = new TextInputDialog("goLogoGo");
                dialog.setTitle("Text Input");
                dialog.setContentText("Change Text:");

                // Traditional way to get the response value.
                Optional<String> result = dialog.showAndWait();
                ((DraggableText) shape).setText(result.toString().substring(9,result.toString().length()-1));
                // Traditional way to get the response value.
                EditText_Transaction editText = new EditText_Transaction(temp,result.toString().substring(9,result.toString().length()-1),app);
                golWorkspace workspace = (golWorkspace) app.getWorkspaceComponent();
                workspace.getJTPS().addTransaction(editText);
                app.getGUI().updateDoToolBarControls(3);
            }
            
        }
    }
    /**
     * Respond to mouse dragging on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMouseDragged(int x, int y) {
        golData dataManager = (golData) app.getDataComponent();
        if (dataManager.isInState(SIZING_SHAPE)) {
            Draggable newDraggableShape = (Draggable) dataManager.getNewShape();
            newDraggableShape.size(x, y);
            app.getGUI().updateDoToolBarControls(3);
        } else if (dataManager.isInState(DRAGGING_SHAPE)) {
            Draggable selectedDraggableShape = (Draggable) dataManager.getSelectedShape();
            selectedDraggableShape.drag(x, y);
            app.getGUI().updateToolbarControls(false);
            app.getGUI().updateDoToolBarControls(3);
        }
    }

    /**
     * Respond to mouse button release on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processCanvasMouseRelease(int x, int y) {
        golData dataManager = (golData) app.getDataComponent();
        if (dataManager.isInState(SIZING_SHAPE)) {
            app.getGUI().updateDuplicationToolbarControls(0);
            dataManager.selectSizedShape();
            MakeNode_Transaction m = new MakeNode_Transaction(dataManager.getSelectedShape(),app);
            golWorkspace workspace = (golWorkspace) app.getWorkspaceComponent();
            workspace.getJTPS().addTransaction(m);
            app.getGUI().updateToolbarControls(false);
        } else if (dataManager.isInState(golState.DRAGGING_SHAPE)) {
            System.out.println("fuh");
            dataManager.setState(SELECTING_SHAPE);
            Scene scene = app.getGUI().getPrimaryScene();
            scene.setCursor(Cursor.DEFAULT);
            app.getGUI().updateToolbarControls(false);
            drag.setNewX(x);
            drag.setNewY(y);
            app.getGUI().updateDoToolBarControls(3);
        } else if (dataManager.isInState(golState.DRAGGING_NOTHING)) {
            dataManager.setState(SELECTING_SHAPE);
        }
    }
    public static double getInitialX(){
        return initialx;
    }
    public static double getInitialY(){
        return initialy;
    }

    public void processModifyFontFamily(String value) {
        golData dataManager = (golData) app.getDataComponent();
        DraggableText selectedDraggableShape = (DraggableText) dataManager.getSelectedShape();
        EditFontFamily_Transaction editFF = new EditFontFamily_Transaction(selectedDraggableShape.getFontFamily(),value,app);
        selectedDraggableShape.setFontFamily(value);
        if (selectedDraggableShape.isBold()){
            if (selectedDraggableShape.isItalic()){
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFontFamily(), FontWeight.BOLD, FontPosture.ITALIC, selectedDraggableShape.getFontSize()));
            } else {
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFontFamily(), FontWeight.BOLD, FontPosture.REGULAR, selectedDraggableShape.getFontSize()));
            }
        } else {
            if (selectedDraggableShape.isItalic()){
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFontFamily(), FontWeight.NORMAL, FontPosture.ITALIC, selectedDraggableShape.getFontSize()));
            } else {
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFontFamily(), FontWeight.NORMAL, FontPosture.REGULAR, selectedDraggableShape.getFontSize()));
            }
        }
        golWorkspace workspace = (golWorkspace) app.getWorkspaceComponent();
        workspace.getJTPS().addTransaction(editFF);
    }

    public void processModifyFontSize(String value) {
        golData dataManager = (golData) app.getDataComponent();
        DraggableText selectedDraggableShape = (DraggableText) dataManager.getSelectedShape();
         EditFontSize_Transaction editFS = new EditFontSize_Transaction(selectedDraggableShape.getFontSize(),Double.parseDouble(value),app);
        selectedDraggableShape.setFontSize(Double.parseDouble(value));
        if (selectedDraggableShape.isBold()){
            if (selectedDraggableShape.isItalic()){
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFontFamily(), FontWeight.BOLD, FontPosture.ITALIC, selectedDraggableShape.getFontSize()));
            } else {
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFontFamily(), FontWeight.BOLD, FontPosture.REGULAR, selectedDraggableShape.getFontSize()));
            }
        } else {
            if (selectedDraggableShape.isItalic()){
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFontFamily(), FontWeight.NORMAL, FontPosture.ITALIC, selectedDraggableShape.getFontSize()));
            } else {
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFontFamily(), FontWeight.NORMAL, FontPosture.REGULAR, selectedDraggableShape.getFontSize()));
            }
        }
        golWorkspace workspace = (golWorkspace) app.getWorkspaceComponent();
        workspace.getJTPS().addTransaction(editFS);
    }

    public void processBolden() {
        golData dataManager = (golData) app.getDataComponent();
        DraggableText selectedDraggableShape = (DraggableText) dataManager.getSelectedShape();
        if (selectedDraggableShape.isBold()){
            if (selectedDraggableShape.isItalic()){
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFontFamily(), FontWeight.NORMAL, FontPosture.ITALIC, selectedDraggableShape.getFontSize()));
            } else {
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFontFamily(), FontWeight.NORMAL, FontPosture.REGULAR, selectedDraggableShape.getFontSize()));
            }
        } else {
            if (selectedDraggableShape.isItalic()){
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFontFamily(), FontWeight.BOLD, FontPosture.ITALIC, selectedDraggableShape.getFontSize()));
            } else {
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFontFamily(), FontWeight.BOLD, selectedDraggableShape.getFontSize()));
            }
        }
        selectedDraggableShape.toggleBold();
        ToggleBold_Transaction editB = new ToggleBold_Transaction(app);
        golWorkspace workspace = (golWorkspace) app.getWorkspaceComponent();
        workspace.getJTPS().addTransaction(editB);
    }

    public void processItalicize() {
        golData dataManager = (golData) app.getDataComponent();
        DraggableText selectedDraggableShape = (DraggableText) dataManager.getSelectedShape();

        if (selectedDraggableShape.isItalic()){
            if (selectedDraggableShape.isBold()){
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFont().getFamily(), FontWeight.BOLD, FontPosture.REGULAR, selectedDraggableShape.getFont().getSize()));
            } else {
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFont().getFamily(), FontPosture.REGULAR, selectedDraggableShape.getFont().getSize()));
            }
        } else {
            if (selectedDraggableShape.isBold()){
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFont().getFamily(), FontWeight.BOLD, FontPosture.ITALIC, selectedDraggableShape.getFont().getSize()));
            } else {
                selectedDraggableShape.setFont(Font.font(selectedDraggableShape.getFont().getFamily(), FontPosture.ITALIC, selectedDraggableShape.getFont().getSize()));
            }
        }
        selectedDraggableShape.toggleItalic();
        ToggleItalicize_Transaction editI; 
        editI = new ToggleItalicize_Transaction(app);
        golWorkspace workspace = (golWorkspace) app.getWorkspaceComponent();
        workspace.getJTPS().addTransaction(editI);
    }
}
