/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import djf.AppTemplate;
import gol.data.DraggableText;
import gol.data.golData;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import jtps.jTPS_Transaction;

/**
 *
 * @author shawn
 */
public class ToggleItalicize_Transaction implements jTPS_Transaction{
    
    AppTemplate app;
    DraggableText text;
    golData dataManager;
    
    public ToggleItalicize_Transaction(AppTemplate initApp){
        app = initApp;
        dataManager = (golData)app.getDataComponent();
        text = (DraggableText)dataManager.getSelectedShape();
    }
    @Override
    public void undoTransaction(){
        System.out.println("why");
        if (text.isItalic()){
            if (text.isBold()){
                text.setFont(Font.font(text.getFont().getFamily(), FontWeight.BOLD, FontPosture.REGULAR, text.getFont().getSize()));
            } else {
                text.setFont(Font.font(text.getFont().getFamily(), FontWeight.NORMAL, FontPosture.REGULAR, text.getFont().getSize()));
            }
        } else {
            if (text.isBold()){
                text.setFont(Font.font(text.getFont().getFamily(), FontWeight.BOLD, FontPosture.ITALIC, text.getFont().getSize()));
            } else {
                text.setFont(Font.font(text.getFont().getFamily(), FontWeight.NORMAL, FontPosture.ITALIC, text.getFont().getSize()));
            }
        }
        text.toggleItalic();
    }
    @Override
    public void doTransaction(){
        if (text.isItalic()){
            if (text.isBold()){
                text.setFont(Font.font(text.getFont().getFamily(), FontWeight.BOLD, FontPosture.REGULAR, text.getFont().getSize()));
            } else {
                text.setFont(Font.font(text.getFont().getFamily(), FontWeight.NORMAL, FontPosture.REGULAR, text.getFont().getSize()));
            }
        } else {
            if (text.isBold()){
                text.setFont(Font.font(text.getFont().getFamily(), FontWeight.BOLD, FontPosture.ITALIC, text.getFont().getSize()));
            } else {
                text.setFont(Font.font(text.getFont().getFamily(), FontWeight.NORMAL, FontPosture.ITALIC, text.getFont().getSize()));
            }
        }
        text.toggleItalic();
    }
}
