/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import djf.AppTemplate;
import gol.data.DraggableEllipse;
import gol.data.DraggableRectangle;
import gol.data.DraggableText;
import gol.data.golData;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author shawn
 */
public class ChangeOT_Transaction implements jTPS_Transaction{
    
    int oldThick;
    int newThick;
    AppTemplate app;
    golData dataManager;
    Node node;
    
    public ChangeOT_Transaction(int oldThic, int newThic, AppTemplate initApp){
        oldThick = oldThic;
        newThick = newThic;
        app = initApp;
        dataManager = (golData)app.getDataComponent();
        node = dataManager.getSelectedShape();
    }
    @Override
    public void undoTransaction(){
        if (node instanceof DraggableText){
            ((DraggableText)node).setStrokeWidth(oldThick);
        } else if (node instanceof DraggableEllipse){
            ((DraggableEllipse)node).setStrokeWidth(oldThick);
        } else if (node instanceof DraggableRectangle){
            ((DraggableRectangle)node).setStrokeWidth(oldThick);
        }
    }
    @Override
    public void doTransaction(){
        if (node instanceof DraggableText){
            ((DraggableText)node).setStrokeWidth(newThick);
        } else if (node instanceof DraggableEllipse){
            ((DraggableEllipse)node).setStrokeWidth(newThick);
        } else if (node instanceof DraggableRectangle){
            ((DraggableRectangle)node).setStrokeWidth(newThick);
        }
    }
    
    public void setNewThick(int thic){
        newThick = thic;
    }
}
