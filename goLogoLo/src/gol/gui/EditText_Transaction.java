/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import djf.AppTemplate;
import gol.data.DraggableText;
import gol.data.golData;
import jtps.jTPS_Transaction;

/**
 *
 * @author shawn
 */
public class EditText_Transaction implements jTPS_Transaction{
    
    String oldText;
    String newText;
    AppTemplate app;
    DraggableText text;
    golData dataManager;
    
    public EditText_Transaction(String oldT, String newT, AppTemplate initApp){
        oldText = oldT;
        newText = newT;
        app = initApp;
        dataManager = (golData)app.getDataComponent();
        text = (DraggableText)dataManager.getSelectedShape();
    }

    EditText_Transaction() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public void undoTransaction(){
        text.setText(oldText);
    }
    @Override
    public void doTransaction(){
        text.setText(newText);
    }
}
