package gol.gui;

import djf.AppTemplate;
import gol.data.golData;
import java.util.Collections;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author shawn
 */
public class MoveFront_Transaction implements jTPS_Transaction{
    Node no;
    
    AppTemplate app;
    golData dataManager;
    int oldplace;
    public MoveFront_Transaction(Node N, AppTemplate initApp){
        no = N;
        app = initApp;
        dataManager = (golData)app.getDataComponent();
        oldplace = dataManager.getShapes().indexOf(N);
    }
    @Override
    public void undoTransaction(){
        //int counter = 0;
        //while (counter < oldplace){
        //    Node lul = dataManager.getShapes().get(counter);
        //    dataManager.getShapes().set(counter,dataManager.getShapes().get(counter + 1));
        //    dataManager.getShapes().set(counter + 1,lul);
        //    //Collections.swap(dataManager.getShapes(), counter, counter-1);
        //    //Collections.swap(dataManager.getShapes(), counter, counter+1);
        //    counter += 1;
        //}
        dataManager.moveShapeToBack(no);
    }
    @Override
    public void doTransaction(){
        dataManager.moveShapeToFront(no);
    }
}
