/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import djf.AppTemplate;
import gol.data.golData;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author shawn
 */
public class Paste_Transaction implements jTPS_Transaction{
    AppTemplate app;
    Node pasted;
    golData dataManager;
    public Paste_Transaction(Node p, AppTemplate initApp){
        pasted = p;
        app = initApp;
        dataManager = ((golData)app.getDataComponent());
    }
    @Override
    public void undoTransaction(){
        dataManager.removeShape(pasted);
    }
    @Override
    public void doTransaction(){
        dataManager.addShape(pasted);
    }
}
