package gol.gui;

import djf.AppTemplate;
import gol.data.golData;
import java.util.Collections;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author shawn
 */
public class MoveBack_Transaction implements jTPS_Transaction{
    Node no;
    
    AppTemplate app;
    golData dataManager;
    int oldplace;
    public MoveBack_Transaction(Node N, AppTemplate initApp){
        no = N;
        app = initApp;
        dataManager = (golData)app.getDataComponent();
        oldplace = dataManager.getShapes().indexOf(N);
    }
    @Override
    public void undoTransaction(){
        //int counter = dataManager.getShapes().size() - 1;
        //while (oldplace < counter && counter > 0){
        //    Node lul = dataManager.getShapes().get(counter);
        //    dataManager.getShapes().set(counter,dataManager.getShapes().get(counter - 1));
        //    dataManager.getShapes().set(counter - 1,lul);
        //    //Collections.swap(dataManager.getShapes(), counter, counter-1);
        //    counter -=1;
        //}
        dataManager.moveShapeToFront(no);
    }
    @Override
    public void doTransaction(){
        dataManager.moveShapeToBack(no);
    }
}
