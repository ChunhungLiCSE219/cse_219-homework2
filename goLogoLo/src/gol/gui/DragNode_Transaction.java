/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import djf.AppTemplate;
import gol.data.DraggableEllipse;
import gol.data.DraggableImageView;
import gol.data.DraggableRectangle;
import gol.data.DraggableText;
import gol.data.golData;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author shawn
 */
public class DragNode_Transaction implements jTPS_Transaction{
    
    double oldX;
    double oldY;
    double newX;
    double newY;
    AppTemplate app;
    golData dataManager;
    Node node;
    
    public DragNode_Transaction(double oldx, double oldy, double newx, double newy, AppTemplate initApp){
        oldX = oldx;
        oldY = oldy;
        newX = newx;
        newY = newy;
        app = initApp;
        dataManager = (golData)app.getDataComponent();
        node = dataManager.getSelectedShape();
    }
    @Override
    public void undoTransaction(){
        if (node instanceof DraggableRectangle){
            ((DraggableRectangle)node).drag((int)oldX, (int)oldY);
        } else if (node instanceof DraggableEllipse){
            ((DraggableEllipse)node).drag((int)oldX, (int)oldY);
        } else if (node instanceof DraggableImageView){
            ((DraggableImageView)node).drag((int)oldX, (int)oldY);
        } else if (node instanceof DraggableText){
            ((DraggableText)node).drag((int)oldX, (int)oldY);
        }
    }
    @Override
    public void doTransaction(){
        System.out.println(newX);
        System.out.println(newY);
        if (node instanceof DraggableRectangle){
            ((DraggableRectangle)node).drag((int)newX, (int)newY);
        } else if (node instanceof DraggableEllipse){
            ((DraggableEllipse)node).drag((int)newX, (int)newY);
        } else if (node instanceof DraggableImageView){
            ((DraggableImageView)node).drag((int)newX, (int)newY);
        } else if (node instanceof DraggableText){
            ((DraggableText)node).drag((int)newX, (int)newY);
        }
    }
    public void setNewX(double newx){
        newX = newx;
    }
    public void setNewY(double newy){
        newY = newy;
    }
}
