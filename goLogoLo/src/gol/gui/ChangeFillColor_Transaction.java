/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import djf.AppTemplate;
import gol.data.DraggableEllipse;
import gol.data.DraggableRectangle;
import gol.data.DraggableText;
import gol.data.golData;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author shawn
 */
public class ChangeFillColor_Transaction implements jTPS_Transaction{
    
    Color oldColor;
    Color newColor;
    AppTemplate app;
    golData dataManager;
    Node node;
    
    public ChangeFillColor_Transaction(Color oldC, Color newC, AppTemplate initApp){
        oldColor = oldC;
        newColor = newC;
        app = initApp;
        dataManager = (golData)initApp.getDataComponent();
        node = dataManager.getSelectedShape();
    }
    
    @Override
    public void undoTransaction(){
        if (node instanceof DraggableText){
            ((DraggableText)node).setFill(oldColor);
        } else if (node instanceof DraggableEllipse){
            ((DraggableEllipse)node).setFill(oldColor);
        } else if (node instanceof DraggableRectangle){
            ((DraggableRectangle)node).setFill(oldColor);
        }
    }
        
    @Override
    public void doTransaction(){
        if (node instanceof DraggableText){
            ((DraggableText)node).setFill(newColor);
        } else if (node instanceof DraggableEllipse){
            ((DraggableEllipse)node).setFill(newColor);
        } else if (node instanceof DraggableRectangle){
            ((DraggableRectangle)node).setFill(newColor);
        }
    }
}
