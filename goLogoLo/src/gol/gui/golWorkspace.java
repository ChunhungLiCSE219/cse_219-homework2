package gol.gui;

import java.io.IOException;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import static gol.golLanguageProperty.*;
import gol.data.golData;
import static gol.data.golData.BLACK_HEX;
import static gol.data.golData.WHITE_HEX;
import gol.data.golState;
import djf.ui.AppYesNoCancelDialogSingleton;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppGUI;
import djf.AppTemplate;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static gol.css.golStyle.*;
import javafx.scene.control.TextArea;
import properties_manager.PropertiesManager;
import static djf.settings.AppPropertyType.*;
import static djf.settings.AppStartupConstants.*;
import properties_manager.PropertiesManager;
import static djf.settings.AppPropertyType.*;
import static djf.settings.AppStartupConstants.*;
import gol.data.DraggableText;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tooltip;
import jtps.jTPS;
import properties_manager.InvalidXMLFileFormatException;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class golWorkspace extends AppWorkspaceComponent {
    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    // HAS ALL THE CONTROLS FOR EDITING
    VBox editToolbar;
    
    // FIRST ROW
    HBox row1Box;
    Button selectionToolButton;
    Button removeButton;
    Button rectButton;
    Button ellipseButton;
    
    // ROW 1.5
    HBox row1point5Box;
    Button imageButton;
    Button textButton;
    
    HBox row1point8Box;
    ComboBox fontFamilyBox;
    ComboBox fontSizeBox;
    Button boldenButton;
    Button italicizeButton;
    
    // SECOND ROW
    HBox row2Box;
    Button moveToBackButton;
    Button moveToFrontButton;

    // THIRD ROW
    VBox row3Box;
    Label backgroundColorLabel;
    ColorPicker backgroundColorPicker;

    // FORTH ROW
    VBox row4Box;
    Label fillColorLabel;
    ColorPicker fillColorPicker;
    
    // FIFTH ROW
    VBox row5Box;
    Label outlineColorLabel;
    ColorPicker outlineColorPicker;
        
    // SIXTH ROW
    VBox row6Box;
    Label outlineThicknessLabel;
    Slider outlineThicknessSlider;
    
    // SEVENTH ROW
    HBox row7Box;
    Button snapshotButton;
   
    
    // THIS IS WHERE WE'LL RENDER OUR DRAWING, NOTE THAT WE
    // CALL THIS A CANVAS, BUT IT'S REALLY JUST A Pane
    Pane canvas;
    
    // HERE ARE THE CONTROLLERS
    CanvasController canvasController;
    LogoEditController logoEditController;    

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;
    
    // FOR DISPLAYING DEBUG STUFF
    Text debugText;
    
    int currentLanguage;
    
    jTPS stack = new jTPS(); 
    
    ChangeOT_Transaction oT;
    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public golWorkspace(AppTemplate initApp) {
	// KEEP THIS FOR LATER
	app = initApp;

	// KEEP THE GUI FOR LATER
	gui = app.getGUI();

        // LAYOUT THE APP
        initLayout();
        
        // HOOK UP THE CONTROLLERS
        initControllers();
        
        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();    
    }
    
    /**
     * Note that this is for displaying text during development.
     */
    public void setDebugText(String text) {
	debugText.setText(text);
    }
    
    // ACCESSOR METHODS FOR COMPONENTS THAT EVENT HANDLERS
    // MAY NEED TO UPDATE OR ACCESS DATA FROM
    
    public ColorPicker getFillColorPicker() {
	return fillColorPicker;
    }
    
    public ColorPicker getOutlineColorPicker() {
	return outlineColorPicker;
    }
    
    public ColorPicker getBackgroundColorPicker() {
	return backgroundColorPicker;
    }
    
    public Slider getOutlineThicknessSlider() {
	return outlineThicknessSlider;
    }

    public Pane getCanvas() {
	return canvas;
    }
    // HELPER SETUP METHOD
    private void initLayout() {
	// THIS WILL GO IN THE LEFT SIDE OF THE WORKSPACE
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	editToolbar = new VBox();
	
	// ROW 1
	row1Box = new HBox();
	selectionToolButton = gui.initChildButton(row1Box, SELECTION_TOOL_ICON.toString(), SELECTION_TOOL_TOOLTIP.toString(), true);
	removeButton = gui.initChildButton(row1Box, REMOVE_ICON.toString(), REMOVE_ELEMENT_TOOLTIP.toString(), true);
	rectButton = gui.initChildButton(row1Box, RECTANGLE_ICON.toString(), RECTANGLE_TOOLTIP.toString(), false);
	ellipseButton = gui.initChildButton(row1Box, ELLIPSE_ICON.toString(), ELLIPSE_TOOLTIP.toString(), false);
        
        // ROW 2
        row1point5Box = new HBox();
        imageButton = gui.initChildButton(row1point5Box, IMAGE_ICON.toString(), IMAGE_TOOLTIP.toString(), false);
        textButton = gui.initChildButton(row1point5Box, TEXT_ICON.toString(), TEXT_TOOLTIP.toString(), false);
        
        row1point8Box = new HBox();
        ObservableList<String> options = 
            FXCollections.observableArrayList(
                    "Verdana",
                    "Georgia",
                    "Courier New",
                    "Times New Roman",
                    "Calibri"
            );
        fontFamilyBox = new ComboBox(options);
        fontFamilyBox.setDisable(true);
        fontFamilyBox.setTooltip(new Tooltip(props.getProperty(FONT_FAMILY_TOOLTIP)));
        ObservableList<String> options2 = 
            FXCollections.observableArrayList(
                    "8",
                    "10",
                    "11",
                    "12",
                    "13",
                    "14",
                    "15",
                    "16",
                    "18",
                    "20",
                    "22",
                    "24",
                    "26",
                    "28",
                    "30",
                    "32",
                    "34",
                    "36",
                    "40",
                    "44",
                    "48",
                    "52",
                    "56",
                    "60",
                    "64"
            );
        fontSizeBox = new ComboBox(options2);
        fontSizeBox.setDisable(true);
        fontSizeBox.setTooltip(new Tooltip(props.getProperty(FONT_SIZE_TOOLTIP)));
        boldenButton = new Button();
        boldenButton.setDisable(true);
        boldenButton.setTooltip(new Tooltip(props.getProperty(BOLDEN_TOOLTIP)));
        boldenButton.setText("B");
        italicizeButton = new Button();
        italicizeButton.setDisable(true);
        italicizeButton.setTooltip(new Tooltip(props.getProperty(ITALICIZE_TOOLTIP)));
        italicizeButton.setText("I");
        row1point8Box.getChildren().add(fontFamilyBox);
        row1point8Box.getChildren().add(fontSizeBox);
        row1point8Box.getChildren().add(boldenButton);
        row1point8Box.getChildren().add(italicizeButton);
        
	// ROW 3
	row2Box = new HBox();
	moveToBackButton = gui.initChildButton(row2Box, MOVE_TO_BACK_ICON.toString(), MOVE_TO_BACK_TOOLTIP.toString(), true);
	moveToFrontButton = gui.initChildButton(row2Box, MOVE_TO_FRONT_ICON.toString(), MOVE_TO_FRONT_TOOLTIP.toString(), true);

	// ROW 4
	row3Box = new VBox();
	backgroundColorLabel = new Label(app.getGUI().getBgColor());
	backgroundColorPicker = new ColorPicker(Color.valueOf(WHITE_HEX));
	row3Box.getChildren().add(backgroundColorLabel);
	row3Box.getChildren().add(backgroundColorPicker);

	// ROW 5
	row4Box = new VBox();
	fillColorLabel = new Label(app.getGUI().getFColor());
	fillColorPicker = new ColorPicker(Color.valueOf(WHITE_HEX));
	row4Box.getChildren().add(fillColorLabel);
	row4Box.getChildren().add(fillColorPicker);
	
	// ROW 6
	row5Box = new VBox();
	outlineColorLabel = new Label(app.getGUI().getOlColor());
	outlineColorPicker = new ColorPicker(Color.valueOf(BLACK_HEX));
	row5Box.getChildren().add(outlineColorLabel);
	row5Box.getChildren().add(outlineColorPicker);
	
	// ROW 7
	row6Box = new VBox();
	outlineThicknessLabel = new Label(app.getGUI().getOtColor());
	outlineThicknessSlider = new Slider(0, 10, 1);
	row6Box.getChildren().add(outlineThicknessLabel);
	row6Box.getChildren().add(outlineThicknessSlider);
	
	// ROW 8
	row7Box = new HBox();
	snapshotButton = gui.initChildButton(row7Box, SNAPSHOT_ICON.toString(), SNAPSHOT_TOOLTIP.toString(), false);
	// NOW ORGANIZE THE EDIT TOOLBAR
	editToolbar.getChildren().add(row1Box);
        editToolbar.getChildren().add(row1point5Box);
        editToolbar.getChildren().add(row1point8Box);
	editToolbar.getChildren().add(row2Box);
	editToolbar.getChildren().add(row3Box);
	editToolbar.getChildren().add(row4Box);
	editToolbar.getChildren().add(row5Box);
	editToolbar.getChildren().add(row6Box);
	editToolbar.getChildren().add(row7Box);
	// WE'LL RENDER OUR STUFF HERE IN THE CANVAS
	canvas = new Pane();
	debugText = new Text();
	canvas.getChildren().add(debugText);
	debugText.setX(100);
	debugText.setY(100);
	
	// AND MAKE SURE THE DATA MANAGER IS IN SYNCH WITH THE PANE
	golData data = (golData)app.getDataComponent();
	data.setShapes(canvas.getChildren());

	// AND NOW SETUP THE WORKSPACE
	workspace = new BorderPane();
	((BorderPane)workspace).setLeft(editToolbar);
	((BorderPane)workspace).setCenter(canvas);
    }
    
    // HELPER SETUP METHOD
    private void initControllers() {
	// MAKE THE EDIT CONTROLLER
	logoEditController = new LogoEditController(app);
	
	// NOW CONNECT THE BUTTONS TO THEIR HANDLERS
	selectionToolButton.setOnAction(e->{
	    logoEditController.processSelectSelectionTool();
	});
	removeButton.setOnAction(e->{
	    logoEditController.processRemoveSelectedShape();
            app.getGUI().updateDoToolBarControls(3);
	});
	rectButton.setOnAction(e->{
	    logoEditController.processSelectRectangleToDraw();
	});
	ellipseButton.setOnAction(e->{
	    logoEditController.processSelectEllipseToDraw();
	});
	
	moveToBackButton.setOnAction(e->{
	    logoEditController.processMoveSelectedShapeToBack();
            app.getGUI().updateDoToolBarControls(3);
	});
	moveToFrontButton.setOnAction(e->{
	    logoEditController.processMoveSelectedShapeToFront();
            app.getGUI().updateDoToolBarControls(3);
	});

	backgroundColorPicker.setOnAction(e->{
	    logoEditController.processSelectBackgroundColor();
            app.getGUI().updateDoToolBarControls(3);
	});
	fillColorPicker.setOnAction(e->{ 
	    logoEditController.processSelectFillColor();
            app.getGUI().updateDoToolBarControls(3);
	});
	outlineColorPicker.setOnAction(e->{
	    logoEditController.processSelectOutlineColor();
            app.getGUI().updateDoToolBarControls(3);
	});
        outlineThicknessSlider.setOnMousePressed(e-> {
            System.out.println("oh");
            golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
            ChangeOT_Transaction ot = new ChangeOT_Transaction(outlineThicknessSlider.valueProperty().intValue(),0,app); 
            oT = ot;
            workspace.getJTPS().addTransaction(ot);
        });
        outlineThicknessSlider.setOnMouseReleased(e-> {
            System.out.println("no");
            golWorkspace workspace = (golWorkspace)app.getWorkspaceComponent();
            oT.setNewThick(outlineThicknessSlider.valueProperty().intValue());
            app.getGUI().updateDoToolBarControls(3);
        });
	outlineThicknessSlider.valueProperty().addListener(e-> {
	    logoEditController.processSelectOutlineThickness();
	});
	snapshotButton.setOnAction(e->{
	    logoEditController.processSnapshot();
	});
        imageButton.setOnAction(e->{
            logoEditController.processEmbedImage();
        });
        textButton.setOnAction(e->{
            logoEditController.processEmbedText();
        });
        app.getGUI().getCopy().setOnAction(e -> {
            logoEditController.processCopy();
            app.getGUI().updateDoToolBarControls(3);
        });
        app.getGUI().getCut().setOnAction(e -> {
            logoEditController.processCut();
            app.getGUI().updateDoToolBarControls(3);
        });
        app.getGUI().getPaste().setOnAction(e -> {
            logoEditController.processPaste();
            app.getGUI().updateDoToolBarControls(3);
        });
	
	// MAKE THE CANVAS CONTROLLER	
	canvasController = new CanvasController(app);
	canvas.setOnMousePressed(e->{
	    canvasController.processCanvasMousePress((int)e.getX(), (int)e.getY());
	});
	canvas.setOnMouseReleased(e->{
	    canvasController.processCanvasMouseRelease((int)e.getX(), (int)e.getY());
	});
	canvas.setOnMouseDragged(e->{
	    canvasController.processCanvasMouseDragged((int)e.getX(), (int)e.getY());
	});
        app.getGUI().getChangeLanguageButton().setOnAction(e->{
            handleChangeLanguageRequest();
        });
        canvas.setOnMouseClicked(e->{
            if (e.getClickCount() == 2){
                System.out.println("what");
                 canvasController.processCanvasMouseDoubleClick((int)e.getX(), (int)e.getY());
            } 
        });
        fontFamilyBox.setOnAction(e->{
            canvasController.processModifyFontFamily((String)fontFamilyBox.getValue());
            app.getGUI().updateDoToolBarControls(3);
        });
        fontSizeBox.setOnAction(e->{
            
            canvasController.processModifyFontSize((String)fontSizeBox.getValue());
            app.getGUI().updateDoToolBarControls(3);
        });
        boldenButton.setOnAction(e->{
            canvasController.processBolden();
            app.getGUI().updateDoToolBarControls(3);
        });
        italicizeButton.setOnAction(e->{
            canvasController.processItalicize();
            app.getGUI().updateDoToolBarControls(3);
        });
        app.getGUI().getUndo().setOnAction(e->{
            handleUndoRequest();
        });
        app.getGUI().getRedo().setOnAction(e->{
            handleRedoRequest();
        });
    }

    // HELPER METHOD
    public void loadSelectedShapeSettings(Shape shape) {
	if (shape != null) {
	    Color fillColor = (Color)shape.getFill();
	    Color strokeColor = (Color)shape.getStroke();
	    double lineThickness = shape.getStrokeWidth();
	    fillColorPicker.setValue(fillColor);
	    outlineColorPicker.setValue(strokeColor);
	    outlineThicknessSlider.setValue(lineThickness);	    
	}
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
	canvas.getStyleClass().add(CLASS_RENDER_CANVAS);
	
	// COLOR PICKER STYLE
	fillColorPicker.getStyleClass().add(CLASS_BUTTON);
	outlineColorPicker.getStyleClass().add(CLASS_BUTTON);
	backgroundColorPicker.getStyleClass().add(CLASS_BUTTON);
	
	editToolbar.getStyleClass().add(CLASS_EDIT_TOOLBAR);
	row1Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row1point5Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
        row1point8Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	row2Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	row3Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	backgroundColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	
	row4Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	fillColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	row5Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	outlineColorLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	row6Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
	outlineThicknessLabel.getStyleClass().add(CLASS_COLOR_CHOOSER_CONTROL);
	row7Box.getStyleClass().add(CLASS_EDIT_TOOLBAR_ROW);
    }
    /**
     * This function reloads all the controls for editing logos
     * the workspace.
     * @param data
     */
    @Override
    public void reloadWorkspace(AppDataComponent data) {
        if (app.getGUI().getLanguage() != currentLanguage){
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            //if (currentLanguage == 1){
            //    System.out.println("oh");
            //    gui.getTemplate().loadProperties(APP_PROPERTIES_EN_FILE_NAME);
            //    currentLanguage = 0;
            //} else {
            //    System.out.println("oh2");
            //    gui.getTemplate().loadProperties(APP_PROPERTIES_ES_FILE_NAME);
            //    currentLanguage = 1;
            //}
         }
	golData dataManager = (golData)data;
	if (dataManager.isInState(golState.STARTING_RECTANGLE)) {
	    selectionToolButton.setDisable(false);
	    removeButton.setDisable(true);
	    rectButton.setDisable(true);
	    ellipseButton.setDisable(false);
	}
	else if (dataManager.isInState(golState.STARTING_ELLIPSE)) {
	    selectionToolButton.setDisable(false);
	    removeButton.setDisable(true);
	    rectButton.setDisable(false);
	    ellipseButton.setDisable(true);
	}
	else if (dataManager.isInState(golState.SELECTING_SHAPE) 
		|| dataManager.isInState(golState.DRAGGING_SHAPE)
		|| dataManager.isInState(golState.DRAGGING_NOTHING)) {
	    boolean shapeIsNotSelected = dataManager.getSelectedShape() == null;
	    selectionToolButton.setDisable(true);
	    removeButton.setDisable(shapeIsNotSelected);
	    rectButton.setDisable(false);
	    ellipseButton.setDisable(false);
	    moveToFrontButton.setDisable(shapeIsNotSelected);
	    moveToBackButton.setDisable(shapeIsNotSelected);
	}
	
	removeButton.setDisable(dataManager.getSelectedShape() == null);
        if (dataManager.getBackgroundColor() == null){
            backgroundColorPicker.setValue(Color.valueOf(WHITE_HEX));
            dataManager.setBackgroundColor(Color.valueOf(WHITE_HEX));
        } else {
            backgroundColorPicker.setValue(dataManager.getBackgroundColor());
        }
        fontFamilyBox.setDisable(!(dataManager.getSelectedShape() instanceof DraggableText));
        fontSizeBox.setDisable(!(dataManager.getSelectedShape() instanceof DraggableText));
        boldenButton.setDisable(!(dataManager.getSelectedShape() instanceof DraggableText));
        italicizeButton.setDisable(!(dataManager.getSelectedShape() instanceof DraggableText));
        
    }
    public void handleChangeLanguageRequest() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        alert.setTitle(props.getProperty(LANGUAGE_TITLE));
        alert.setContentText(props.getProperty(LANGUAGE_CONTEXT));

        ButtonType buttonTypeOne = new ButtonType(props.getProperty(ENGLISH_LABEL));
        ButtonType buttonTypeTwo = new ButtonType(props.getProperty(SPANISH_LABEL));
        ButtonType buttonTypeCancel = new ButtonType(props.getProperty(CANCEL_LABEL), ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeCancel);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne){
            loadProperties(APP_PROPERTIES_EN_FILE_NAME);
        } else if (result.get() == buttonTypeTwo) {
            loadProperties(APP_PROPERTIES_ES_FILE_NAME);
        } else {
        }
        gui.setBgColor(props.getProperty(BACKGROUND_COLOR_LABEL));
        gui.setFColor(props.getProperty(FILL_COLOR_LABEL));
        gui.setOlColor(props.getProperty(OUTLINE_COLOR_LABEL));
        gui.setOtColor(props.getProperty(OUTLINE_THICKNESS_LABEL));
        selectionToolButton.setTooltip(new Tooltip(props.getProperty(SELECTION_TOOL_TOOLTIP)));
        removeButton.setTooltip(new Tooltip(props.getProperty(REMOVE_ELEMENT_TOOLTIP)));
        rectButton.setTooltip(new Tooltip(props.getProperty(RECTANGLE_TOOLTIP)));
        ellipseButton.setTooltip(new Tooltip(props.getProperty(ELLIPSE_TOOLTIP)));
        moveToBackButton.setTooltip(new Tooltip(props.getProperty(MOVE_TO_BACK_TOOLTIP)));
        moveToFrontButton.setTooltip(new Tooltip(props.getProperty(MOVE_TO_FRONT_TOOLTIP)));
        backgroundColorPicker.setTooltip(new Tooltip(props.getProperty(BACKGROUND_TOOLTIP)));
        fillColorPicker.setTooltip(new Tooltip(props.getProperty(FILL_TOOLTIP)));
        outlineColorPicker.setTooltip(new Tooltip(props.getProperty(OUTLINE_TOOLTIP)));
        snapshotButton.setTooltip(new Tooltip(props.getProperty(SNAPSHOT_TOOLTIP)));
        imageButton.setTooltip(new Tooltip(props.getProperty(IMAGE_TOOLTIP)));
        textButton.setTooltip(new Tooltip(props.getProperty(TEXT_TOOLTIP)));
        app.getGUI().getNewButton().setTooltip(new Tooltip(props.getProperty(NEW_TOOLTIP)));
        app.getGUI().getLoadButton().setTooltip(new Tooltip(props.getProperty(LOAD_TOOLTIP)));
        app.getGUI().getSaveButton().setTooltip(new Tooltip(props.getProperty(SAVE_TOOLTIP)));
        app.getGUI().getExitButton().setTooltip(new Tooltip(props.getProperty(EXIT_TOOLTIP)));
        app.getGUI().getChangeLanguageButton().setTooltip(new Tooltip(props.getProperty(CHANGE_LANGUAGE_TOOLTIP)));
        app.getGUI().getAboutButton().setTooltip(new Tooltip(props.getProperty(ABOUT_TOOLTIP)));
        backgroundColorLabel.setText(props.getProperty(BACKGROUND_COLOR_LABEL));
        fillColorLabel.setText(props.getProperty(FILL_COLOR_LABEL));
        outlineColorLabel.setText(props.getProperty(OUTLINE_COLOR_LABEL));
        outlineThicknessLabel.setText(props.getProperty(OUTLINE_THICKNESS_LABEL));
    }
    public void handleUndoRequest(){
        stack.undoTransaction();
    }
    public void handleRedoRequest(){
        stack.doTransaction();

    }
    public boolean loadProperties(String propertiesFileName) {
	    PropertiesManager props = PropertiesManager.getPropertiesManager();
	try {
	    // LOAD THE SETTINGS FOR STARTING THE APP
	    props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
	    props.loadProperties(propertiesFileName, PROPERTIES_SCHEMA_FILE_NAME);
	    return true;
	} catch (InvalidXMLFileFormatException ixmlffe) {
	    // SOMETHING WENT WRONG INITIALIZING THE XML FILE
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(PROPERTIES_LOAD_ERROR_TITLE), props.getProperty(PROPERTIES_LOAD_ERROR_MESSAGE));
	    return false;
	}
    }
    @Override
    public void resetWorkspace() {
        stack.reset();
        app.getGUI().updateDuplicationToolbarControls(3);
        app.getGUI().updateDoToolBarControls(0);
    }
    public jTPS getJTPS(){
        return stack;
    }
    public LogoEditController getLogoEditController(){
        return logoEditController;
    }
}