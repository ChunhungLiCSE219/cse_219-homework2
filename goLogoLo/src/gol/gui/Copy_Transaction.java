/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import djf.AppTemplate;
import gol.data.golData;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author shawn
 */
public class Copy_Transaction implements jTPS_Transaction{
    
    Node oldCopy;
    Node newCopy;
    AppTemplate app;
    LogoEditController editor;
    
    public Copy_Transaction(Node oldCop, Node newCop, AppTemplate initApp){
        oldCopy = oldCop;
        newCopy = newCop;
        app = initApp;
        editor = ((golWorkspace)app.getWorkspaceComponent()).getLogoEditController();
    }
    @Override
    public void undoTransaction(){
        editor.setClipboard(oldCopy);
    }
    @Override
    public void doTransaction(){
        editor.setClipboard(newCopy);
    }
    public void setNewCopy(Node newest){
        newCopy = newest;
    }
}
