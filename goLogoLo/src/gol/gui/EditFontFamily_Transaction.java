/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import djf.AppTemplate;
import gol.data.DraggableText;
import gol.data.golData;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import jtps.jTPS_Transaction;

/**
 *
 * @author shawn
 */
public class EditFontFamily_Transaction implements jTPS_Transaction{
    
    String oldFont;
    String newFont;
    AppTemplate app;
    DraggableText text;
    golData dataManager;
    
    public EditFontFamily_Transaction(String oldF, String newF, AppTemplate initApp){
        oldFont = oldF;
        newFont = newF;
        app = initApp;
        dataManager = (golData)app.getDataComponent();
        text = (DraggableText)dataManager.getSelectedShape();
    }
    @Override
    public void undoTransaction(){
        text.setFontFamily(oldFont);
        if (text.isBold()){
            if (text.isItalic()){
                text.setFont(Font.font(text.getFontFamily(), FontWeight.BOLD, FontPosture.ITALIC, text.getFontSize()));
            } else {
                text.setFont(Font.font(text.getFontFamily(), FontWeight.BOLD, FontPosture.REGULAR, text.getFontSize()));
            }
        } else {
            if (text.isItalic()){
                text.setFont(Font.font(text.getFontFamily(), FontWeight.NORMAL, FontPosture.ITALIC, text.getFontSize()));
            } else {
                text.setFont(Font.font(text.getFontFamily(), FontWeight.NORMAL, FontPosture.REGULAR, text.getFontSize()));
            }
        }
    }
    @Override
    public void doTransaction(){
        text.setFontFamily(newFont);
        if (text.isBold()){
            if (text.isItalic()){
                text.setFont(Font.font(text.getFontFamily(), FontWeight.BOLD, FontPosture.ITALIC, text.getFontSize()));
            } else {
                text.setFont(Font.font(text.getFontFamily(), FontWeight.BOLD, FontPosture.REGULAR, text.getFontSize()));
            }
        } else {
            if (text.isItalic()){
                text.setFont(Font.font(text.getFontFamily(), FontWeight.NORMAL, FontPosture.ITALIC, text.getFontSize()));
            } else {
                text.setFont(Font.font(text.getFontFamily(), FontWeight.NORMAL, FontPosture.REGULAR, text.getFontSize()));
            }
        }
    }
}
