/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import djf.AppTemplate;
import gol.data.golData;
import javafx.scene.Node;
import jtps.jTPS_Transaction;

/**
 *
 * @author shawn
 */
public class MakeNode_Transaction implements jTPS_Transaction{
    AppTemplate app;
    Node make;
    golData dataManager;
    public MakeNode_Transaction(Node m,AppTemplate initApp){
        make = m;
        app = initApp;
        dataManager = (golData)app.getDataComponent();
    }
    @Override
    public void undoTransaction(){
        dataManager.removeShape(make);
    }
    @Override
    public void doTransaction(){
        dataManager.addShape(make);
    }
    
}
