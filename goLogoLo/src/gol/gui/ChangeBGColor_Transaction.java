/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gol.gui;

import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import djf.*;
import gol.data.golData;
/**
 *
 * @author shawn
 */
public class ChangeBGColor_Transaction implements jTPS_Transaction{
    
    Color oldColor;
    Color newColor;
    AppTemplate app;
    golData dataManager;
    
    public ChangeBGColor_Transaction(Color oldC, Color newC, AppTemplate initApp){
        oldColor = oldC;
        newColor = newC;
        app = initApp;
        dataManager = (golData)initApp.getDataComponent();
    }
    @Override
    public void undoTransaction(){
        dataManager.setBackgroundColor(oldColor);
    }
    @Override
    public void doTransaction(){
        dataManager.setBackgroundColor(newColor);
    }
}
