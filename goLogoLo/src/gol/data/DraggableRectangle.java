package gol.data;

import gol.gui.CanvasController;
import javafx.scene.shape.Rectangle;

/**
 * This is a draggable rectangle for our goLogoLo application.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DraggableRectangle extends Rectangle implements Draggable {
    double startX;
    double startY;
    
    public DraggableRectangle() {
	setX(0.0);
	setY(0.0);
	setWidth(0.0);
	setHeight(0.0);
	setOpacity(1.0);
	startX = 0.0;
	startY = 0.0;
    }
    
    @Override
    public golState getStartingState() {
	return golState.STARTING_RECTANGLE;
    }
    
    @Override
    public void start(int x, int y) {
	startX = (double)x;
	startY = (double)y;
    }
    
    @Override
    public void drag(int x, int y) {
        double diffX = x - startX;
        double diffY = y - startY;
        double newX = startX + diffX;
	double newY = startY + diffY;
        if (xProperty().doubleValue() + diffX> 0 
                && xProperty().doubleValue() + getWidth() + diffX< 1560 
                && yProperty().doubleValue() + diffY> 0 
                && yProperty().doubleValue() + getHeight() + diffY< 900){
            xProperty().set(newX - CanvasController.getInitialX());
            yProperty().set(newY - CanvasController.getInitialY());
            startX = newX;
            startY = newY;
        }
    }
    
    public String cT(double x, double y) {
	return "(x,y): (" + x + "," + y + ")";
    }
    
    public double getStartX(){
        return startX;
    }
    
    public double getStartY(){
        return startY;
    }
    
    @Override
    public void size(int x, int y) {
	double width = x - getStartX();
	widthProperty().set(width);
	double height = y - getStartY();
	heightProperty().set(height);
        
    }
    
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
	xProperty().set(initX);
	yProperty().set(initY);
	widthProperty().set(initWidth);
	heightProperty().set(initHeight);
    }
    
    @Override
    public String getShapeType() {
	return RECTANGLE;
    }
    public DraggableRectangle deepCopy(double x, double y){
        DraggableRectangle copy = new DraggableRectangle();
        copy.setLocationAndSize(x,y,getWidth(),getHeight());
        copy.setFill(getFill());
        copy.setStroke(getStroke());
        copy.setStrokeWidth(getStrokeWidth());
        return copy;
    }
}
