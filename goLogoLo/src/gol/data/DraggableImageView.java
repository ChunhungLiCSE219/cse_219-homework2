package gol.data;

import gol.gui.CanvasController;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Paint;

/**
 * This interface represents a family of draggable shapes.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DraggableImageView extends ImageView implements Draggable{
    double startCenterX;
    double startCenterY;
    private String path;
    
    public DraggableImageView(Image img){
        setImage(img);
        setFitWidth(img.getWidth());
        setFitHeight(img.getHeight());
    }
    @Override
    public golState getStartingState(){
        return golState.STARTING_IMAGE;
    }
    
    @Override
    public void start(int x, int y) {
        startCenterX = x;
        startCenterY = y;
    }
    
    @Override
    public void drag(int x, int y) {
        double diffX = x - startCenterX;
        double diffY = y - startCenterY;
        double newX = startCenterX + diffX;
	double newY = startCenterY + diffY;
        System.out.println("newX: " + newX);
        System.out.println("newY: " + newY);
        if (xProperty().doubleValue() + diffX> 0 
                && xProperty().doubleValue() + getWidth() + diffX< 1560 
                && yProperty().doubleValue() + diffY> 0 
                && yProperty().doubleValue() + getHeight() + diffY< 900){
            xProperty().set(newX - CanvasController.getInitialX());
            yProperty().set(newY - CanvasController.getInitialY());
            startCenterX = newX;
            startCenterY = newY;
        }
    }
    
    @Override
    public void size(int x, int y) {
        double width = x - getX();
	setFitWidth(width);
	double height = y - getY();
	setFitHeight(height);	
    }
        
    public double getCenterX() {
        return startCenterX;
    }

    public double getCenterY() {
        return startCenterY;
    }

    @Override
    public double getWidth() {
        return getFitWidth();
    }

    @Override
    public double getHeight() {
        return getFitHeight();
    }
        
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight) {
        xProperty().set(initX);
	yProperty().set(initY);
	setFitWidth(initWidth);
	setFitHeight(initHeight);
    }
    
    
    
    @Override
    public String getShapeType(){
        return IMAGEVIEW;
    }
    
    public DraggableImageView deepCopy(double x, double y){
        Image temp = getImage();
        DraggableImageView copy = new DraggableImageView(temp);
        copy.startCenterX = x;
        copy.startCenterY = y;
        return copy;
    }
    
    public void setPath(String s){
        path = s;
    }
    
    public String getPath(){
        return path;
    }
}
