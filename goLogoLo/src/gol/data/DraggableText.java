package gol.data;

import gol.gui.CanvasController;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;

/**
 * This interface represents a family of draggable shapes.
 * 
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DraggableText extends Text implements Draggable{
    double startCenterX;
    double startCenterY;
    private boolean bold;
    private boolean italic;
    private String fontFamily;
    private Double fontSize;
    public DraggableText(){
        
    }
    public DraggableText(String text){
        setText(text);
        bold = false;
        italic = false;
        fontFamily = "Verdana";
        fontSize = 12.0;
    }
    
    @Override
    public golState getStartingState(){
        return golState.STARTING_TEXT;
    }
    @Override
    public void start(int x, int y){
        startCenterX = x;
        startCenterY = y;
    }
    @Override
    public void drag(int x, int y){
        double diffX = x - startCenterX;
        double diffY = y - startCenterY;
        double newX = startCenterX + diffX;
	double newY = startCenterY + diffY;
        if (newX > 0 && newX + getWidth()< 1560 && newY> 0 && newY + getHeight() < 900){
            xProperty().set(newX - CanvasController.getInitialX());
            yProperty().set(newY - CanvasController.getInitialY());
            startCenterX = newX;
            startCenterY = newY;
        }
    }
    
    public void size(int x, int y){
        
    }
    
    public void changeFont(String fontFamily, int size, boolean bold, boolean italics){
        setFont(Font.font(fontFamily,size));
        if (bold && italics){
            setFont(Font.font(fontFamily, FontWeight.BOLD, FontPosture.ITALIC, size));
        } else if (bold && !italics){
            setFont(Font.font(fontFamily, FontWeight.BOLD, size));
        } else if (!bold && italics){
            setFont(Font.font(fontFamily, FontPosture.ITALIC, size));
        } else {
           setFont(Font.font(fontFamily, size));
        }
    }
    @Override
    public double getWidth(){
        return getWrappingWidth();
    }
    @Override
    public double getHeight(){
        return 0;
    }
    @Override
    public void setLocationAndSize(double initX, double initY, double initWidth, double initHeight){
        xProperty().set(initX);
	yProperty().set(initY);
    }
    @Override
    public String getShapeType(){
        return TEXT;
    }
    public boolean isBold(){
        return bold;
    }
    public boolean isItalic(){
        return italic;
    }
    public void toggleBold(){
        bold = !bold;
    }
    public void toggleItalic(){
        italic = !italic;
    }
    public void setFontFamily(String ff){
        fontFamily = ff;
    }
    public void setFontSize(Double fs){
        fontSize = fs;
    }
    public String getFontFamily(){
        return fontFamily;
    }
    public Double getFontSize(){
        return fontSize;
    }
    public DraggableText deepCopy(double x, double y){
        DraggableText copy = new DraggableText(getText());
        copy.setFont(getFont());
        copy.start((int)x,(int)y);
        copy.setFill(getFill());
        copy.setStroke(getStroke());
        copy.setStrokeWidth(getStrokeWidth());
        return copy;
    }
}
